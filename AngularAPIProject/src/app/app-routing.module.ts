import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddproductComponent } from './components/addproduct/addproduct.component';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DeleteproductComponent } from './components/deleteproduct/deleteproduct.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UpdateproductComponent } from './components/updateproduct/updateproduct.component';

const routes: Routes = [
 { path:'register',component:RegisterComponent},
 {path:'login',component:LoginComponent},
 {path:'dashboard',component:DashboardComponent},
 {path:'adminlogin',component:AdminloginComponent},
 {path:'admindashboard',component:AdmindashboardComponent},
 {path:'addproduct',component:AddproductComponent},
 {path:'updateproduct/:id',component:UpdateproductComponent},
 {path:'deleteproduct/:id',component:DeleteproductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
