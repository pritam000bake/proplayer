import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  product:Product;

  constructor(private productService:ProductService,private router:Router) { 
    console.log('within add product componant');
    this.product= new Product()

  }

  ngOnInit(): void {

  }
  addProduct(product:Product) {
        // this.user.name="msd"; 
        // this.user.password="msd";
        // this.user.location="Pune";
        // this.user.email="msd@gmail.com";
        // this.user.isblocked=false;
        console.log(product)
        this.productService.addProduct(product).subscribe(res=>{
          if(res){
            console.log("Add Product Sucess");
            Swal.fire(
              'add product',
              'add product success',
              'success'
            )
            this.router.navigateByUrl("/admindashboard")
            
            }
          else{
            console.log("Add Product failed");
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!',
            })
          }
        });
      }

}

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { User } from 'src/app/models/user';
// import { UserService } from 'src/app/service/user.service';
// import Swal from 'sweetalert2';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent implements OnInit {
//   user:User;

//   constructor(private userService:UserService,private router:Router) { 
//     console.log('within register Componants');
//     this.user= new User()
//   }

//   ngOnInit(): void {
//     // this.registerUser();
//   }
//   

// }

